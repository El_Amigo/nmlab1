#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "function/cosex.h"
#include "function/testfunction.h"

#include <vector>
#include <algorithm>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->tableViewSpline->setModel(&m_splineData);
    ui->tableViewPrecision->setModel(&m_interpData);

    // initialize axis range (and scroll bar positions via signals we just connected):
    ui->plot->xAxis->setRange(-5.0, 5.0);
    ui->plot->yAxis->setRange(-5.0, 5.0);
    ui->plot->yAxis->setScaleRatio(ui->plot->xAxis, 1.0);

    ui->plot->axisRect()->setupFullAxesBox(true);
    ui->plot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);

    ui->plot->setLocale(QLocale(QLocale::English, QLocale::UnitedKingdom)); // period as decimal separator and comma as thousand separator
    QFont legendFont = font();  // start out with MainWindow's font..
    legendFont.setPointSize(9); // and make a bit smaller for legend
    ui->plot->legend->setFont(legendFont);
    ui->plot->legend->setBrush(QBrush(QColor(255,255,255,230)));
    // by default, the legend is in the inset layout of the main axis rect. So this is how we access it to change legend placement:
    ui->plot->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignBottom|Qt::AlignRight);

    // Connect button signal to appropriate slot
    connect(ui->pushButtonSpline, SIGNAL (released()), this, SLOT (on_pushButtonSpline_released()));
    connect(ui->buttonGroup, SIGNAL(buttonReleased(int)), this, SLOT(Plot(int)));

    connect(ui->buttonGroup_2, SIGNAL(buttonReleased(int)), &m_splineData, SLOT(onBoundaryConditionChanged(int)));
    connect(ui->comboBoxFunction, SIGNAL(currentIndexChanged(int)), &m_splineData, SLOT(onFunctionChanged(int)));
    connect(ui->spinBoxIntervalsNum, SIGNAL(valueChanged(int)), &m_splineData, SLOT(onIntervalsNumChanged(int)));

    connect(&m_splineData, SIGNAL(dataChanged(QModelIndex, QModelIndex)), ui->tableViewSpline, SLOT(dataChanged(QModelIndex, QModelIndex)));

    ui->buttonGroup->setId(ui->radioButtonFunction, static_cast<int>(Value::Function));
    ui->buttonGroup->setId(ui->radioButton1derivaitve, static_cast<int>(Value::FirstDerivative));
    ui->buttonGroup->setId(ui->radioButton2derivaitve, static_cast<int>(Value::SecondDerivative));

    ui->buttonGroup_2->setId(ui->radioButtonNullBoundary, static_cast<int>(BoundaryCondition::Natural));
    ui->buttonGroup_2->setId(ui->radioButtonSecondDerivative, static_cast<int>(BoundaryCondition::EqualSecondDerivatives));

    emit ui->buttonGroup_2->buttonReleased(ui->buttonGroup_2->checkedId());
    emit ui->comboBoxFunction->currentIndexChanged(ui->comboBoxFunction->currentIndex());
    emit ui->spinBoxIntervalsNum->valueChanged(ui->spinBoxIntervalsNum->value());
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButtonSpline_released()
{
    m_current_func = ui->comboBoxFunction->currentText();
    m_splineData.Refresh();
    m_interpData.SetData(std::move(m_splineData.GetInterpolationData()));
    Plot(ui->buttonGroup->checkedId());
    ShowMax();
}

void MainWindow::Plot(int derv_num)
{

    QString function_name("F");
    QString spline_name("S");
    QString dervivative = (derv_num ? QString(derv_num, '\'') : "");
    function_name += dervivative + "(x)";
    spline_name += dervivative + "(x)";
    ui->plot->legend->setVisible(!m_interpData.GetData().m_mesh.empty());
    ui->plot->clearGraphs();
    ui->plot->addGraph();
    ui->plot->graph()->setPen(QPen(Qt::blue));
    ui->plot->graph()->setData(QVector<double>::fromStdVector(m_interpData.GetData().m_mesh)
                               , QVector<double>::fromStdVector(m_interpData.GetData().m_derivative[derv_num].functionValues), true);
    ui->plot->graph()->setName(derv_num ? QString("(") + m_current_func + ")" + dervivative : m_current_func);
    ui->plot->addGraph();
    ui->plot->graph()->setPen(QPen(Qt::green));
    ui->plot->graph()->setData(QVector<double>::fromStdVector(m_interpData.GetData().m_mesh)
                               , QVector<double>::fromStdVector(m_interpData.GetData().m_derivative[derv_num].splineValues), true);
    ui->plot->graph()->setName(spline_name);
    ui->plot->addGraph();
    ui->plot->graph()->setPen(QPen(Qt::red));
    ui->plot->graph()->setData(QVector<double>::fromStdVector(m_interpData.GetData().m_mesh)
                               , QVector<double>::fromStdVector(m_interpData.GetData().m_derivative[derv_num].errorValues), true);
    ui->plot->graph()->setName(QString("|") + function_name + "-" + spline_name + "|");


    ui->plot->replot(QCustomPlot::rpImmediateRefresh);
    ui->plot->xAxis->setRange(-5.0, 5.0);
    ui->plot->yAxis->setRange(-5.0, 5.0);
}

void MainWindow::ShowMax()
{
    ui->plainTextEditmaxF->document()->setPlainText(QString::number(m_interpData.GetData().m_derivative[0].maxError.second));
    ui->plainTextEditmaxFcoo->document()->setPlainText(QString::number(m_interpData.GetData().m_derivative[0].maxError.first));
    ui->plainTextEditmaxFx->document()->setPlainText(QString::number(m_interpData.GetData().m_derivative[1].maxError.second));
    ui->plainTextEditmaxFxcoo->document()->setPlainText(QString::number(m_interpData.GetData().m_derivative[1].maxError.first));
    ui->plainTextEditmaxFxx->document()->setPlainText(QString::number(m_interpData.GetData().m_derivative[2].maxError.second));
    ui->plainTextEditmaxFxxcoo->document()->setPlainText(QString::number(m_interpData.GetData().m_derivative[2].maxError.first));
}
