#pragma once
#include <QAbstractTableModel>
#include <vector>

typedef std::pair<double,double> Point;

struct InterpolationData
{
    struct DataValues
    {
        std::vector<double> functionValues;
        std::vector<double> splineValues;
        std::vector<double> errorValues;
        Point maxError;
    };
    std::vector<double> m_mesh;
    DataValues m_derivative[3];
};


class InterpolationDataModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    InterpolationDataModel(QObject* parent = 0);
    ~InterpolationDataModel() {}
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    const InterpolationData& GetData() { return m_data;}
    void SetData(const InterpolationData data);
private:
    InterpolationData m_data;
};
