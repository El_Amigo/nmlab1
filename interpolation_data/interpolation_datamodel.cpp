#include "interpolation_datamodel.h"

enum class InterpDataColumns
{
    kMesh = 0,
    kFunction = 1,
    kSpline = 2,
    kFunctionDiff = 3,
    kDerivative = 4,
    kSplineDerivative = 5,
    kDerivativeDiff = 6,
    kCount,
};

InterpolationDataModel::InterpolationDataModel(QObject *parent)
    :QAbstractTableModel(parent)
{
}

int InterpolationDataModel::rowCount(const QModelIndex & /*parent*/) const
{
   return m_data.m_mesh.size();
}

int InterpolationDataModel::columnCount(const QModelIndex & /*parent*/) const
{
    return static_cast<int>(InterpDataColumns::kCount);
}

QVariant InterpolationDataModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole)
    {
        switch(orientation)
        {
            case Qt::Horizontal:
            {
                switch(section)
                {
                    case static_cast<int>(InterpDataColumns::kMesh):
                        return "x";
                    break;
                    case static_cast<int>(InterpDataColumns::kFunction):
                        return "F(x)";
                    break;
                    case static_cast<int>(InterpDataColumns::kSpline):
                        return "S(x)";
                    break;
                    case static_cast<int>(InterpDataColumns::kFunctionDiff):
                        return "|F(x) - S(x)|";
                    break;
                    case static_cast<int>(InterpDataColumns::kDerivative):
                        return "F'(x)";
                    break;
                    case static_cast<int>(InterpDataColumns::kSplineDerivative):
                        return "S'(x)";
                    break;
                    case static_cast<int>(InterpDataColumns::kDerivativeDiff):
                        return "|F'(x) - S'(x)|";
                    break;
                }
                break;
            }
            case Qt::Vertical:
            {
                return section + 1;
                break;
            }
        }
    }
    return QVariant();
}

QVariant InterpolationDataModel::data(const QModelIndex &index, int role) const
{
    switch (role)
    {
       case Qt::DisplayRole:
           switch(index.column())
           {
               case static_cast<int>(InterpDataColumns::kMesh):
                   return m_data.m_mesh[index.row()];
               break;
               case static_cast<int>(InterpDataColumns::kFunction):
                   return m_data.m_derivative[0].functionValues[index.row()];
               break;
               case static_cast<int>(InterpDataColumns::kSpline):
                   return m_data.m_derivative[0].splineValues[index.row()];
               break;
               case static_cast<int>(InterpDataColumns::kFunctionDiff):
                   return m_data.m_derivative[0].errorValues[index.row()];
               break;
               case static_cast<int>(InterpDataColumns::kDerivative):
                   return m_data.m_derivative[1].functionValues[index.row()];
               break;
               case static_cast<int>(InterpDataColumns::kSplineDerivative):
                   return m_data.m_derivative[1].splineValues[index.row()];
               break;
               case static_cast<int>(InterpDataColumns::kDerivativeDiff):
                   return m_data.m_derivative[1].errorValues[index.row()];
               break;
           }
       break;
       case Qt::TextAlignmentRole:
       {
           return Qt::AlignCenter;
       }
       break;
    }
    return QVariant();
}

void InterpolationDataModel::SetData(const InterpolationData data)
{
    beginResetModel();
    m_data = data;
    endResetModel();
}
