#-------------------------------------------------
#
# Project created by QtCreator 2016-09-29T00:55:07
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = NMLab1
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    qcustomplot/qcustomplot.cpp \
    spline/spline.cpp \
    function/function.cpp \
    spline/spline_datamodel.cpp \
    interpolation_data/interpolation_datamodel.cpp

HEADERS  += mainwindow.h \
    qcustomplot/qcustomplot.h \
    function/function.h \
    function/cosex.h \
    function/testfunction.h \
    spline/spline.h \
    spline/spline_datamodel.h \
    interpolation_data/interpolation_datamodel.h

FORMS    += mainwindow.ui
