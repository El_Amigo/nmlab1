#pragma once

#include <vector>
#include <function/function.h>

enum class BoundaryCondition
{
    Natural,
    EqualSecondDerivatives,
};

struct Segment
{
    struct Coefficients
    {
        double a,b,c,d;
    };
    Coefficients coeff;
    double mesh_point;
};

class Spline
{
public:
    Spline(const FunctionBase& func, BoundaryCondition bcond, size_t segments_num, double spline_begin, double spline_end);
    template<Value>
    double GetValue(const Segment&, double x) const;
    template<Value>
    std::vector<double> GetValues(const std::vector<double>& mesh) const;
    std::vector<double> GetValues(Value valuestoGet, const std::vector<double>& mesh) const;
    std::vector<Segment> m_segments;
};

template<Value valueType>
std::vector<double> Spline::GetValues(const std::vector<double>& mesh) const
{
    std::vector<double> points;
    points.reserve(mesh.size());
    auto it_seg = ++m_segments.begin();
    for(const auto& x : mesh)
    {
        while (x > it_seg->mesh_point)
        {
            it_seg++;
        }
        double value = GetValue<valueType>(*it_seg, x);
        points.push_back(value);
    }
    return points;
}
