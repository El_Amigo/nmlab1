#include "spline_datamodel.h"
#include "function/cosex.h"
#include "function/testfunction.h"

#include <cmath>

enum class SplineDataColumns
{
    kSegmentBegin = 0,
    kSegmentEnd = 1,
    kA = 2,
    kB = 3,
    kC = 4,
    kD = 5,
    kCount,
};

SplineDataModel::SplineDataModel(QObject *parent)
    :QAbstractTableModel(parent)
{
}

int SplineDataModel::rowCount(const QModelIndex & /*parent*/) const
{
   return m_intervals_num;
}

int SplineDataModel::columnCount(const QModelIndex & /*parent*/) const
{
    return static_cast<int>(SplineDataColumns::kCount);
}

QVariant SplineDataModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole)
    {
        switch (orientation)
        {
            case Qt::Horizontal:
            {
                switch(section)
                {
                    case static_cast<int>(SplineDataColumns::kSegmentBegin):
                        return "Xi";
                    break;
                    case static_cast<int>(SplineDataColumns::kSegmentEnd):
                        return "Xi+1";
                    break;
                    case static_cast<int>(SplineDataColumns::kA):
                        return "Ai";
                    break;
                    case static_cast<int>(SplineDataColumns::kB):
                        return "Bi";
                    break;
                    case static_cast<int>(SplineDataColumns::kC):
                        return "Ci";
                    break;
                    case static_cast<int>(SplineDataColumns::kD):
                        return "Di";
                    break;
                }
                break;
            }
            case Qt::Vertical:
            {
                return section + 1;
                break;
            }
        }
    }
    return QVariant();
}

QVariant SplineDataModel::data(const QModelIndex &index, int role) const
{
    switch (role)
    {
       case Qt::DisplayRole:
           switch(index.column())
           {
               case static_cast<int>(SplineDataColumns::kSegmentBegin):
                   return m_spline->m_segments[index.row()].mesh_point;
               break;
               case static_cast<int>(SplineDataColumns::kSegmentEnd):
                   return m_spline->m_segments[index.row() + 1].mesh_point;
               break;
               case static_cast<int>(SplineDataColumns::kA):
                   return m_spline->m_segments[index.row() + 1].coeff.a;
               break;
               case static_cast<int>(SplineDataColumns::kB):
                   return m_spline->m_segments[index.row() + 1].coeff.b;
               break;
               case static_cast<int>(SplineDataColumns::kC):
                   return m_spline->m_segments[index.row() + 1].coeff.c;
               break;
               case static_cast<int>(SplineDataColumns::kD):
                   return m_spline->m_segments[index.row() + 1].coeff.d;
               break;
           }
        break;
        case Qt::TextAlignmentRole:
        {
            return Qt::AlignCenter;
        }
        break;
    }
    return QVariant();
}

void SplineDataModel::onBoundaryConditionChanged(int index)
{
    m_bcond = static_cast<BoundaryCondition>(index);
}

void SplineDataModel::onFunctionChanged(int index)
{
    switch(index)
    {
        case 0:
            m_function.reset(new TestFunction());
        break;
        case 1:
            m_function.reset(new CosEx());
        break;
        case 2:
            m_function.reset(new CosExCos10X());
        break;
        case 3:
            m_function.reset(new CosExCos100X());
        break;
    }
}

void SplineDataModel::onIntervalsNumChanged(int intervals_num)
{
    m_intervals_num = intervals_num;
}

void SplineDataModel::Refresh()
{
    beginResetModel();
    m_spline.reset(new Spline(*m_function, m_bcond, m_intervals_num, -1.0, 1.0));
    endResetModel();
}

const InterpolationData SplineDataModel::GetInterpolationData() const
{
    InterpolationData data;
    double delta = 0.005;//(m_spline->m_segments[1].mesh_point - m_spline->m_segments[0].mesh_point) / 4.0;
    data.m_mesh.reserve(static_cast<size_t>((m_spline->m_segments.back().mesh_point - m_spline->m_segments.front().mesh_point) / delta));
    for (double x = m_spline->m_segments.front().mesh_point; x < m_spline->m_segments.back().mesh_point; x += delta)
    {
        data.m_mesh.push_back(x);
    }
    for (size_t i = 0; i < static_cast<size_t>(Value::kCount); i++)
    {
        data.m_derivative[i].functionValues = m_function->GetValues(static_cast<Value>(i), data.m_mesh);
        data.m_derivative[i].splineValues = m_spline->GetValues(static_cast<Value>(i), data.m_mesh);
        data.m_derivative[i].errorValues.resize(data.m_mesh.size());
        std::transform(data.m_derivative[i].functionValues.begin()
                       , data.m_derivative[i].functionValues.end()
                       , data.m_derivative[i].splineValues.begin()
                       , data.m_derivative[i].errorValues.begin()
                       , [](const double& lhs, const double& rhs) -> double { return std::abs(lhs - rhs);});
        size_t index = std::distance(data.m_derivative[i].errorValues.begin()
                                     , std::max_element(data.m_derivative[i].errorValues.begin(), data.m_derivative[i].errorValues.end()));
        data.m_derivative[i].maxError = index < data.m_derivative[i].errorValues.size()
                                      ? std::make_pair(data.m_mesh[index], data.m_derivative[i].errorValues[index])
                                      : std::make_pair(0.0, 0.0);
    }
    return data;
}
