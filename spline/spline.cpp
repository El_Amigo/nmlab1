#include "spline.h"

#include <algorithm>

Spline::Spline(const FunctionBase& func, BoundaryCondition bcond,size_t segments_num, double spline_begin, double spline_end)
  : m_segments(segments_num + 1)
{
    double interval = (spline_end - spline_begin) / segments_num;

    for(size_t i = 0; i < m_segments.size();  i++)
    {
        double x = spline_begin + interval * i;
        m_segments[i].mesh_point = x;
        m_segments[i].coeff.a = func.GetValue(x);
    }

    double mu1, mu2;
    switch(bcond)
    {
        case BoundaryCondition::Natural:
            mu1 = 0.0;
            mu2 = 0.0;
            break;
        case BoundaryCondition::EqualSecondDerivatives:
            mu1 = func.GetSecondDerivativeValue(spline_begin);
            mu2 = func.GetSecondDerivativeValue(spline_end);
        break;
        default:
            throw std::exception("No such boundary condition found!");
    }

    std::vector<double> alpha(segments_num, 0.0), beta(segments_num, 0.0);

    alpha[0] = 0.0;

    for(size_t i = 1; i < alpha.size(); i++)
    {
        
    }
    //
    beta[0] = mu1;
    for(size_t i = 1; i < segments_num;  i++)
    {
        double A = m_segments[i].mesh_point - m_segments[i - 1].mesh_point;
        double B = m_segments[i + 1].mesh_point - m_segments[i].mesh_point;
        double C = 2.0 * (A + B);

        alpha[i] = -B / (C + A * alpha[i - 1]);

        double fiI = 6.0 / A * ( m_segments[i - 1].coeff.a
                            - 2.0 * m_segments[i].coeff.a
                            + m_segments[i + 1].coeff.a );
        beta[i] = (fiI - A * beta[i - 1]) / (C + alpha[i - 1] * A);
    }

    m_segments.front().coeff.c = mu1;
    m_segments.back().coeff.c = mu2;
    for(size_t i = segments_num; i > 0;  i--)
    {
        m_segments[i - 1].coeff.c = alpha[i - 1] * m_segments[i].coeff.c + beta[i - 1];
    }

    for(size_t i = segments_num; i > 0;  i--)
    {
        m_segments[i].coeff.d = (m_segments[i].coeff.c - m_segments[i - 1].coeff.c) / (m_segments[i].mesh_point - m_segments[i - 1].mesh_point);
    }

    for(auto it = ++m_segments.begin(); it != m_segments.end(); it++)
    {
        it->coeff.b = (it->mesh_point - (it - 1)->mesh_point) * (2.0 * it->coeff.c + (it - 1)->coeff.c) / 6.0 + (it->coeff.a - (it - 1)->coeff.a) / (it->mesh_point - (it - 1)->mesh_point);
    }
}

template <>
double Spline::GetValue<Value::Function>(const Segment& segment, double x) const
{
    return segment.coeff.a
            + segment.coeff.b * (x - segment.mesh_point)
            + segment.coeff.c / 2.0 * (x - segment.mesh_point) * (x - segment.mesh_point)
            + segment.coeff.d / 6.0 * (x - segment.mesh_point) * (x - segment.mesh_point) * (x - segment.mesh_point);
}

template <>
double Spline::GetValue<Value::FirstDerivative>(const Segment& segment, double x) const
{
    return segment.coeff.b
            + segment.coeff.c * (x - segment.mesh_point)
            + segment.coeff.d / 2.0 * (x - segment.mesh_point) * (x - segment.mesh_point);
}

template <>
double Spline::GetValue<Value::SecondDerivative>(const Segment& segment, double x) const
{
    return segment.coeff.c + segment.coeff.d * (x - segment.mesh_point);
}

std::vector<double> Spline::GetValues(Value valuestoGet, const std::vector<double>& mesh) const
{
    switch(valuestoGet)
    {
        case Value::Function:
            return GetValues<Value::Function>(mesh);
        break;
        case Value::FirstDerivative:
            return GetValues<Value::FirstDerivative>(mesh);
        break;
        case Value::SecondDerivative:
            return GetValues<Value::SecondDerivative>(mesh);
        break;
    }
    return std::vector<double>();
}

//const std::vector<Segment>& GetSegments()
//{
//    return m_segments;
//}
