#pragma once
#include "spline/spline.h"
#include "interpolation_data/interpolation_datamodel.h"

#include <QAbstractTableModel>
#include <memory>

struct FunctionBase;

class SplineDataModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    SplineDataModel(QObject* parent = 0);
    ~SplineDataModel() {}
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;

    void Refresh();
    const InterpolationData GetInterpolationData() const;
public slots:
    void onBoundaryConditionChanged(int index);
    void onFunctionChanged(int index);
    void onIntervalsNumChanged(int intervals_num);
private:
    std::unique_ptr<Spline> m_spline;
    std::unique_ptr<FunctionBase> m_function;
    BoundaryCondition m_bcond;
    int m_intervals_num = 0;
};
