#pragma once

#include "interpolation_data/interpolation_datamodel.h"
#include "spline/spline_datamodel.h"

#include <QMainWindow>
#include <memory>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private:
    void ShowMax();
private slots:
    void Plot(int rbutton_ind);
    void on_pushButtonSpline_released();
private:
    InterpolationDataModel m_interpData;
    SplineDataModel m_splineData;
    QString m_current_func;
    Ui::MainWindow* ui;
};
