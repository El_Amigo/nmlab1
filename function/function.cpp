#include "function.h"
#include <assert.h>

template <>
double FunctionBase::GetValue<Value::Function>(double x) const
{
    return GetValue(x);
}

template <>
double FunctionBase::GetValue<Value::FirstDerivative>(double x) const
{
    return GetFristDerivativeValue(x);
}

template <>
double FunctionBase::GetValue<Value::SecondDerivative>(double x) const
{
    return GetSecondDerivativeValue(x);
}

std::vector<double> FunctionBase::GetValues(Value valuestoGet, const std::vector<double>& mesh) const
{
    switch(valuestoGet)
    {
        case Value::Function:
            return GetValues<Value::Function>(mesh);
        break;
        case Value::FirstDerivative:
            return GetValues<Value::FirstDerivative>(mesh);
        break;
        case Value::SecondDerivative:
            return GetValues<Value::SecondDerivative>(mesh);
        break;
    }
    return std::vector<double>();
}

std::string FunctionBase::GetDerivativeString(unsigned int derv_num) const
{
    return std::string("(") + GetFunctionString() + ")" + (derv_num ? std::string(derv_num, '\'') : "");
}
