#pragma once

#include "function.h"

#include <cmath>

class CosEx : public FunctionBase
{
public:
    ~CosEx() override {}
    double GetValue(double x) const override { return cos(exp(x));}
    double GetFristDerivativeValue(double x) const override { return - exp(x) * sin(exp(x));}
    double GetSecondDerivativeValue(double x) const override { return - exp(x) * sin (exp(x)) - exp(2.0 * x) * cos(exp(x)); }
    std::string GetFunctionString() const override{ return "cos(exp(x))";}
};


class CosExCos10X : public CosEx
{
public:
    ~CosExCos10X() override {}
    double GetValue(double x) const override { return CosEx::GetValue(x) + cos(1e1 * x);}
    double GetFristDerivativeValue(double x) const override { return CosEx::GetFristDerivativeValue(x) - 1.e1 * sin(1.e1 * x);}
    double GetSecondDerivativeValue(double x) const override { return CosEx::GetSecondDerivativeValue(x) - 1.e2 * cos(1.e1 * x); }
    std::string GetFunctionString() const override{ return CosEx::GetFunctionString() + " + cos(10x)";}
};

class CosExCos100X : public CosEx
{
public:
    ~CosExCos100X() override {}
    double GetValue(double x) const override { return CosEx::GetValue(x) + cos(1.e2 * x);}
    double GetFristDerivativeValue(double x) const override { return CosEx::GetFristDerivativeValue(x) - 1.e2 * sin(1.e2 * x);}
    double GetSecondDerivativeValue(double x) const override { return CosEx::GetSecondDerivativeValue(x) - 1.e4 * cos(1.e2 * x); }
    std::string GetFunctionString() const override{ return CosEx::GetFunctionString() + " + cos(100x)";}
};
