#pragma once

#include <string>
#include <vector>

enum class Value
{
    Function = 0,
    FirstDerivative = 1,
    SecondDerivative = 2,
    kCount,
};

struct FunctionBase
{
public:
    virtual ~FunctionBase() {}
    template<Value>
    double GetValue(double x) const;
    template<Value>
    std::vector<double> GetValues(const std::vector<double>& x) const;
    virtual std::string GetFunctionString() const = 0;
    std::string GetDerivativeString(unsigned int derv_num) const;
    virtual double GetValue(double x) const = 0;
    virtual double GetFristDerivativeValue(double x) const = 0;
    virtual double GetSecondDerivativeValue(double x) const = 0;
    std::vector<double> GetValues(Value valuestoGet, const std::vector<double>& mesh) const;
};


template<Value ValueToGet>
std::vector<double> FunctionBase::GetValues(const std::vector<double>& x) const
{
    std::vector<double> values;
    values.reserve(x.size());

    for(const auto& it : x)
    {
        values.push_back(GetValue<ValueToGet>(it));
    }

    return values;
}
