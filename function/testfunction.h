#pragma once

#include "function.h"

#include <cmath>

class TestFunction : public FunctionBase
{
public:
    ~TestFunction() override {}
    double GetValue(double x) const override { return x > 0.0 ? x * x * ( 3.0 - x) : (x * x) * ( 3.0 + x);}
    double GetFristDerivativeValue(double x) const override { return x > 0.0 ? 3.0 * x * (2.0 - x) : 3.0 * x * (2.0 + x);}
    double GetSecondDerivativeValue(double x) const override { return x > 0.0 ? 6 * (1.0 - x) : 6 * (1.0 + x); }
    std::string GetFunctionString() const override{ return "x^3 + 3x^2";}
};
